/*
Package cleanup provides a unified way to defer running functions until a later date.

Cleanup provides a way for tooling to defer running functions until a later date. A typical example would be wanting to defer running a shutdown function until main() exits:

func shutdown() {
	// shutdown function for some service that should really be run before the
	// program exits
	...
}

cleanup.Add(shutdown)

func main() {
	defer cleanup.Run() // Ensure that any cleanup functions are run on exit
	...
}

*/
package cleanup

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

import (
	"fmt"
	"sync"
)

// Runable is the interface satisfied by an object with a Run method.
type Runable interface {
	Run() error
}

// Group is a collection of Runables that should be run together.
type Group struct {
	Name string     // The group's name
	m    sync.Mutex // Controls access to the following
	fs   []Runable  // The Runables
}

// defaultGroup is the default group. Treat this variable as a constant.
var defaultGroup *Group

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init creates the default Runable group.
func init() {
	defaultGroup = &Group{Name: "Default"}
}

// execRunable executes the given Runable, recovering from any panic.
func execRunable(r Runable) (err error) {
	// Recover from any panics caused by r
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("Panic in Runable: %s", e)
		}
	}()
	// Execute the runable
	err = r.Run()
	return
}

/////////////////////////////////////////////////////////////////////////
// runableFunc
/////////////////////////////////////////////////////////////////////////

// runableFunc wraps a func() error to make it runable.
type runableFunc func() error

// Run executes the function.
func (f runableFunc) Run() error {
	return f()
}

/////////////////////////////////////////////////////////////////////////
// Group functions
/////////////////////////////////////////////////////////////////////////

// String returns the group name.
func (g *Group) String() string {
	if g == nil {
		return ""
	}
	return g.Name
}

// Add adds the given function to the group. This will panic if g or f are nil.
func (g *Group) Add(f func() error) {
	if f == nil {
		panic("Illegal nil function")
	}
	g.AddRunable(runableFunc(f))
}

// AddRunable adds the given Runable to the group. This will panic if g is nil.
func (g *Group) AddRunable(r Runable) {
	if g == nil {
		panic("Uninitialised Group")
	}
	g.m.Lock()
	if g.fs == nil {
		g.fs = make([]Runable, 0, 5)
	}
	g.fs = append(g.fs, r)
	g.m.Unlock()
}

// Run executes the Runables in the group, in LIFO order. Runables will be removed from the group as they are executed. It is safe for a Runable to add itself back to the group to be executed at a later date. The first error encountered (if any) will be returned, however all Runables will be run.
func (g *Group) Run() error {
	if g == nil {
		return nil
	}
	g.m.Lock()
	fs := g.fs
	g.fs = nil
	g.m.Unlock()
	var err error
	for i := len(fs) - 1; i >= 0; i-- {
		if e := execRunable(fs[i]); err == nil {
			err = e
		}
	}
	return err
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Add adds the given function. This will panic if f is nil.
func Add(f func() error) {
	defaultGroup.Add(f)
}

// AddRunable adds the given Runable.
func AddRunable(r Runable) {
	defaultGroup.AddRunable(r)
}

// Run executes the Runables, in LIFO order. Runables will be removed as they are executed. It is safe for a Runable to add itself back to be executed at a later date. The first error encountered (if any) will be returned, however all Runables will be run.
func Run() error {
	return defaultGroup.Run()
}
