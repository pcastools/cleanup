// Signal provides a signal handler for the cleanup package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cleanup

import (
	"context"
	"os"
	"os/signal"
)

// Printer is the interface satisfied by the Printf method.
type Printer interface {
	Printf(format string, v ...interface{})
}

// CancelOnSignal installs a signal handler, calling the given cancel function on the first os.Interrupt. Subsequent os.Interrupt signals will cause immediate termination.
func CancelOnSignal(cancel context.CancelFunc, lg Printer) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		if lg != nil {
			lg.Printf("Received interrupt signal")
		}
		signal.Stop(c)
		cancel()
	}()
}
